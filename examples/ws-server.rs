extern crate ws_endpoint;

use serde::{Deserialize, Serialize};
use std::io;
use ws_endpoint::WsEndpoint;

use std::time::Duration;
use tokio::time::delay_for;

#[derive(Debug, Serialize, Deserialize)]
enum ApiRequest {
    SayHello,
    Greet(String),
    Add(Vec<i32>),
    Wait(usize),
}

#[derive(Debug, Serialize, Deserialize)]
enum ApiResponse {
    Hello,
    Greet(String),
    Sum(i32),
    Waited(usize),
}

#[tokio::main]
async fn main() -> io::Result<()> {
    env_logger::init();

    let endpoint = WsEndpoint::new_sync_text_endpoint(process);
    endpoint.start("127.0.0.1:5000").await
}

async fn process(request: String) -> Option<String> {
    let request: ApiRequest = serde_json::from_str(&request).unwrap();

    let response = match request {
        ApiRequest::SayHello => ApiResponse::Hello,
        ApiRequest::Greet(someone) => ApiResponse::Greet(format!("Hello, {}!", someone)),
        ApiRequest::Add(is) => ApiResponse::Sum(is.iter().sum()),
        ApiRequest::Wait(i) => {
            delay_for(Duration::from_secs(i as u64)).await;
            ApiResponse::Waited(i)
        }
    };

    Some(serde_json::to_string(&response).unwrap())
}
