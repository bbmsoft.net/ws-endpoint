extern crate ws_endpoint;

use std::io;
use ws_endpoint::WsEndpoint;

#[tokio::main]
async fn main() -> io::Result<()> {
    let endpoint = WsEndpoint::new_sync_text_endpoint(process);
    endpoint.start("127.0.0.1:5000").await
}

async fn process(request: String) -> Option<String> {
    // We are just echoing here.
    // In a real world scenario this would be
    // the place to parse and process the
    // request and return a proper response.
    let response = request;

    Some(response)
}
